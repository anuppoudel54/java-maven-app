def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'nexus-docker-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t 34.206.164.133:8083/java-maven-app:1.2 .'
        sh 'echo $PASS | docker login -u $USER --password-stdin 34.206.164.133:8083'
        sh 'docker push 34.206.164.133:8083/java-maven-app:1.2'
    }
} 

def deployApp() {
    echo 'deploying the application...'
    
} 

return this